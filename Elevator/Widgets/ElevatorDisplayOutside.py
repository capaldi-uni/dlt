from ..UI import Ui_ElevatorDisplayOutside
from ..Components import Intention

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal
from PyQt5.Qt import Qt


class ElevatorDisplayOutside(QWidget, Ui_ElevatorDisplayOutside):
    button_clicked: pyqtBoundSignal = pyqtSignal()

    _arrows = {
        Intention.STOP: Qt.NoArrow,
        Intention.UP: Qt.UpArrow,
        Intention.DOWN: Qt.DownArrow,
    }

    def __init__(self, parent=None, button_text='Enter'):
        super().__init__(parent=parent)

        self.setupUi(self)

        self.button.setText(button_text)
        self.button.clicked.connect(self.button_clicked)

        self.direction_display.setStyleSheet(
            "QToolButton, QToolButton:hover, QToolButton:pressed {"
            "   border: none"
            "}"
        )

    def close_doors(self):
        self.button.setDisabled(True)

    def open_doors(self):
        self.button.setEnabled(True)

    def display(self, intention: Intention):
        self.floor_display.display(intention.floor + 1)

        self.direction_display.setArrowType(
            self._arrows[intention.direction]
        )
