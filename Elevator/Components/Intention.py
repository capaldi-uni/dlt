class Intention:
    STOP = 0
    UP = 1
    DOWN = 2

    _dirs = {
        STOP: 'stop',
        UP: 'up',
        DOWN: 'down',
    }

    _floor: int
    _direction: int

    def __init__(self, floor, direction):
        self._floor = floor
        self._direction = direction

    @property
    def floor(self) -> int:
        return self._floor

    @property
    def direction(self) -> int:
        return self._direction

    @property
    def dir(self):
        return self._dirs[self._direction]

    @property
    def is_moving(self):
        return self._direction > Intention.STOP

    @property
    def is_stop(self):
        return self._direction == Intention.STOP

    @property
    def is_up(self):
        return self._direction == Intention.UP

    @property
    def is_down(self):
        return self._direction == Intention.DOWN

    def __str__(self):
        return f'{self.floor}|{self.dir}'
