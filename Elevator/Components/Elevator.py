from .SystemWithWindow import SystemWithWindow

from ..Windows import ElevatorWindow
from ..Components import Intention, Settings

from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal, QTimer


class Elevator(SystemWithWindow):
    # Components
    window: ElevatorWindow

    # Signals
    floor_requested: pyqtBoundSignal = pyqtSignal(int)
    elevator_exited: pyqtBoundSignal = pyqtSignal(int)

    elevator_arrived: pyqtBoundSignal = pyqtSignal(int)
    elevator_departed: pyqtBoundSignal = pyqtSignal(int)

    intention_changed: pyqtBoundSignal = pyqtSignal(Intention)

    # Data
    number: int
    current_floor: int = 0
    _intention: Intention

    def __init__(self, parent=None, *,
                 number: int = 0,
                 floor_count: int = 0):

        super().__init__(ElevatorWindow(), parent)

        self.number = number

        self.window.set_number(number)

        self.window.floor_requested.connect(
            self.floor_requested
        )

        self.setup_floors(floor_count)

        self.window.elevator_exited.connect(self._exit_elevator)
        self.intention_changed.connect(self.window.display_intention)

    def setup_floors(self, count: int):
        for i in range(count):
            self.window.add_floor_button(Settings.floors_per_row)

    def _exit_elevator(self):
        self.elevator_exited.emit(self.current_floor)

    def depart(self):
        self.window.close_doors()

        self.elevator_departed.emit(self.current_floor)

    def arrive(self, floor):
        self.current_floor = floor

        self.window.set_floor_selected(floor, False)
        self.window.open_doors()

        self.intention = Intention(self.current_floor, Intention.STOP)

        self.elevator_arrived.emit(self.current_floor)

    @property
    def intention(self) -> Intention:
        return self._intention

    @intention.setter
    def intention(self, value: Intention):
        self._intention = value

        self.intention_changed.emit(self._intention)
