
# Static
class Settings:
    # System
    floor_count = 12
    elevator_count = 3

    elevator_speed = 0.1

    # UI
    close_on_exit = True
    floors_per_row = 3
    elevators_per_row = 3
