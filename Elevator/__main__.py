from . import app
from .Components import ControlPanel, Settings

cs = ControlPanel(
    floor_count=Settings.floor_count,
    elevator_count=Settings.elevator_count
).start()

app.exec()
