from ..UI import Ui_ElevatorWindow
from ..Widgets import ElevatorDisplayOutside
from ..Components import Intention, Settings

from .window_mixins import NoCloseMixin, HasFloorsMixin

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal

from functools import partial


class ElevatorWindow(QMainWindow, Ui_ElevatorWindow,
                     HasFloorsMixin, NoCloseMixin):
    # Components
    elevator_display: ElevatorDisplayOutside

    # Signals
    elevator_exited: pyqtBoundSignal = pyqtSignal()

    def __init__(self):
        super().__init__()

        self.setupUi(self)

        self.__mixinit__()

        self.elevator_display = ElevatorDisplayOutside(self, 'Exit')
        self.layout_0.addWidget(self.elevator_display)

        self.elevator_display.button_clicked.connect(self.elevator_exited)
        self.floor_requested.connect(self.select_floor)

        self.elevator_exited.connect(self.on_exit)

    def set_number(self, num: int):
        self.elevator_number.setText(str(num + 1))
        self.setWindowTitle(f'Elevator {num + 1}')

    def set_floor_selected(self, floor: int, selected: bool):
        self.floor_buttons[floor].setDisabled(selected)

    def select_floor(self, floor: int):
        self.set_floor_selected(floor, True)

    def deselect_floor(self, floor: int):
        self.set_floor_selected(floor, False)

    def open_doors(self):
        self.elevator_display.open_doors()

    def close_doors(self):
        self.elevator_display.close_doors()

    def display_intention(self, intention: Intention):
        self.elevator_display.display(intention)

    # Experimental
    def on_exit(self):
        if Settings.close_on_exit:
            self.hide()
